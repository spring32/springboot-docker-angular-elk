## Before you start
* Install Docker and Docker Compose.
* Change environment variable values in .env file for more security or leave it as it is.
* Make sure to build the project: mvn package [-DskipTests]

## Production mode
In this mode, all latest images will be pulled from Docker Hub. Just copy *docker-compose.yml* and hit *docker-compose up*

## Development mode
If you'd like to build images yourself (with some changes in the code, for example), you have to clone all repository and build artifacts with maven. 
Then, run *docker-compose -f docker-compose.yml -f docker-compose.dev.yml up*

*docker-compose.dev.yml* inherits *docker-compose.yml* with additional possibility to build images locally and expose all containers ports for convenient development.
If you'd like to start applications in Intellij Idea you need to either use EnvFile plugin or manually export environment 
variables listed in .env file (make sure they were exported: printenv).

By default, the stack exposes the following ports:

**5044**: Logstash Beats input
**5000**: Logstash TCP input
**9600**: Logstash monitoring API
**9200**: Elasticsearch HTTP
**9300**: Elasticsearch TCP transport
**5601**: Kibana