package com.eara.microservices.app.students.dtos;

import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * This class represents the data structure used
 * to transfer student information between the frontend
 * and backend.
 *
 * @author Ernesto A. Rodriguez Acosta
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class StudentDTO implements Serializable {

    private static final long serialVersionUID = 900742494245142511L;

    private Long id;

    private String firstName;
    private String lastName;
    private String email;
    private Date createdAt;
}