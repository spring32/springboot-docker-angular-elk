package com.eara.microservices.app.students.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This class is used to specify the Response Status
 * for an specific exception along with the definition
 * of the Exception.
 *
 * @author Ernesto A. Rodriguez Acosta
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends Exception {

    private static final long serialVersionID = 1L;

    public ResourceNotFoundException(String message) {
        super(message);
    }
}
