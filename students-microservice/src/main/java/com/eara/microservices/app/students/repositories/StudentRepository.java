package com.eara.microservices.app.students.repositories;

import com.eara.microservices.app.students.domains.Student;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface StudentRepository extends CrudRepository<Student, Long> {

    Optional<Student> findStudentByIdAndFirstName(Long studentId, String firstName);
}
