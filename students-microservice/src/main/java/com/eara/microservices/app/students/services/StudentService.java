package com.eara.microservices.app.students.services;

import com.eara.microservices.app.students.dtos.StudentDTO;
import com.eara.microservices.app.students.exceptions.ResourceNotFoundException;

import java.util.List;
import java.util.Optional;

public interface StudentService {

    /**
     * Returns the student list.
     * @return student list.
     */
    List<StudentDTO> findAll();

    /**
     * Return the student with that id .
     * @param studentId student ID
     * @return an student.
     */
    Optional<StudentDTO> findStudentById(Long studentId);

    /**
     * Return the student with that id and firstName.
     * @param studentId student ID
     * @param firstName student firstname
     * @return an student.
     */
    Optional<StudentDTO> findStudentByIdAndFirstName(Long studentId, String firstName);

    /**
     * To create a new student.
     * @param studentDTO student DTO
     * @return the saved student DTO.
     */
    StudentDTO saveStudent(StudentDTO studentDTO);

    /**
     * To update a new student.
     * @param studentId student ID
     * @param studentDTO student DTO
     * @return the DTO with the student info.
     */
    Optional<StudentDTO> updateStudent(Long studentId, StudentDTO studentDTO);

    /**
     * Delete the student with that id.
     * @param studentId student id to delete.
     * @throws ResourceNotFoundException
     */
    void deleteStudentById(Long studentId) throws ResourceNotFoundException;
}
