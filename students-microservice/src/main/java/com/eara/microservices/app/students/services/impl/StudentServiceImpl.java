package com.eara.microservices.app.students.services.impl;

import com.eara.microservices.app.students.domains.Student;
import com.eara.microservices.app.students.dtos.StudentDTO;
import com.eara.microservices.app.students.exceptions.ResourceNotFoundException;
import com.eara.microservices.app.students.repositories.StudentRepository;
import com.eara.microservices.app.students.services.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@Transactional(
        isolation = Isolation.READ_COMMITTED,
        propagation = Propagation.SUPPORTS,
        timeout = 30
)
public class StudentServiceImpl implements StudentService {

    private StudentRepository studentRepository;

    /**
     * Returns the student list.
     * @return student list.
     */
    @Transactional(readOnly = true)
    @Override
    public List<StudentDTO> findAll() {
        log.info("Finding all students");

        List<Student> students =(List<Student>) studentRepository.findAll();

        log.info("Foud {} students", students.size());

        return new ModelMapper().map(students,
                new TypeToken<List<StudentDTO>>() {}.getType());
    }

    /**
     * Return the student with that id .
     * @param studentId student ID
     * @return an student.
     */
    @Transactional(readOnly = true)
    @Override
    public Optional<StudentDTO> findStudentById(Long studentId) {
        log.info("Finding student with id {}", studentId);

        Optional<Student> student = studentRepository.findById(studentId);

        log.info("Founded {} student", student.isPresent() ? 1 : 0);

        return student.isPresent() ? Optional.of(new ModelMapper().map(student, StudentDTO.class))
                : Optional.empty();
    }

    /**
     * Return the student with that id and firstName.
     * @param studentId student ID
     * @param firstName student firstname
     * @return an student.
     */
    @Transactional(readOnly = true)
    @Override
    public Optional<StudentDTO> findStudentByIdAndFirstName(Long studentId, String firstName) {
        Optional<Student> student = studentRepository.findStudentByIdAndFirstName(studentId, firstName);

        return student.isPresent() ? Optional.of(new ModelMapper().map(student, StudentDTO.class))
                : Optional.empty();
    }

    /**
     * To create a new student.
     * @param studentDTO student DTO
     * @return the saved student DTO.
     */
    @Transactional(
            rollbackFor = Exception.class,
            noRollbackFor = ResourceNotFoundException.class)
    @Override
    public StudentDTO saveStudent(StudentDTO studentDTO) {
        ModelMapper modelMapper = new ModelMapper();
        Student storedStudent =studentRepository.save(
                modelMapper.map(studentDTO, Student.class));

        return modelMapper.map(storedStudent, StudentDTO.class);
    }

    /**
     * To update a new student.
     * @param studentId student ID
     * @param studentDTO student DTO
     * @return the DTO with the student info.
     */
    @Transactional(
            rollbackFor = Exception.class,
            noRollbackFor = ResourceNotFoundException.class)
    @Override
    public Optional<StudentDTO> updateStudent(Long studentId, StudentDTO studentDTO) {
        Optional<Student> optionalStudent = studentRepository.findById(studentId);

        if (optionalStudent.isPresent()) {
            ModelMapper modelMapper = new ModelMapper();
            Student student = Student.builder()
                    .id(optionalStudent.get().getId())
                    .firstName(optionalStudent.get().getFirstName())
                    .lastName(optionalStudent.get().getLastName())
                    .email(optionalStudent.get().getEmail())
                    .createdAt(optionalStudent.get().getCreatedAt())
                    .build();

            Student updatedStudent = studentRepository.save(student);

            return Optional.of(modelMapper.map(updatedStudent, StudentDTO.class));
        }
        else {
            return Optional.empty();
        }
    }

    /**
     * Delete the student with that id.
     * @param studentId student id to delete.
     * @throws ResourceNotFoundException
     */
    @Transactional(
            rollbackFor = {Exception.class, ResourceNotFoundException.class})
    @Override
    public void deleteStudentById(Long studentId) throws ResourceNotFoundException {
        Student student = studentRepository.findById(studentId)
                .orElseThrow(() -> new ResourceNotFoundException(
                        new StringBuilder().append("Student not found for id: ")
                                .append(studentId)
                                .toString()));

        studentRepository.deleteById(studentId);
    }
}
