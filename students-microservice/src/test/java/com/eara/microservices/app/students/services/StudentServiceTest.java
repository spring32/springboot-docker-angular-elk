package com.eara.microservices.app.students.services;

import com.eara.microservices.app.students.domains.Student;
import com.eara.microservices.app.students.dtos.StudentDTO;
import com.eara.microservices.app.students.exceptions.ResourceNotFoundException;
import com.eara.microservices.app.students.repositories.StudentRepository;
import com.eara.microservices.app.students.services.impl.StudentServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class StudentServiceTest {

    @Mock
    private StudentRepository studentRepository;

    @InjectMocks
    private StudentServiceImpl studentService;

    @BeforeEach
    public void initUseCase() {

    }

    @Test
    @Order(1)
    public void shouldReturnAnStudentList() {
        List<Student> students = Arrays.asList(
                Student.builder().id(1L).firstName("Ernesto").lastName("Acosta").email("").build(),
                Student.builder().id(1L).firstName("Paola").lastName("Damiani").email("").build());

        BDDMockito.given(studentRepository.findAll()).willReturn(students);

        // when
        List<StudentDTO> allStudents = studentService.findAll();

        // then
        Assertions.assertThat(allStudents.size()).isEqualTo(2);
    }

    @Test
    @Order(2)
    public void shouldReturnStudentWithThatId() {
        Student student = Student.builder()
                .id(1L)
                .firstName("Ernesto")
                .lastName("Acosta")
                .email("")
                .build();

        Optional<Student> studentOptional = Optional.of(student);

        BDDMockito.given(studentRepository
                .findById(1L))
                .willReturn(studentOptional);

        // when
        Optional<?> studentDTO = studentService.findStudentById(1L);

        // then
        Assertions.assertThat(studentDTO).isNotEmpty();
    }

    @Test
    @Order(3)
    public void shouldReturnOptionalEmptyForInexistentStudentWithThatId() {

        BDDMockito.given(studentRepository
                .findById(2L))
                .willReturn(Optional.empty());

        // when
        Optional<StudentDTO> studentDTO = studentService.findStudentById(2L);

        // then
        Assertions.assertThat(studentDTO).isEqualTo(Optional.empty());
    }

    @Test
    @Order(4)
    public void shouldReturnStudentWithThatIdAndFirstname() {
        Student student = Student.builder()
                .id(1L)
                .firstName("Ernesto")
                .lastName("Acosta")
                .email("")
                .build();

        Optional<Student> studentOptional = Optional.of(student);

        BDDMockito.given(studentRepository
                .findStudentByIdAndFirstName(student.getId(), student.getFirstName()))
                .willReturn(studentOptional);

        // when
        Optional<?> studentDTO = studentService.findStudentByIdAndFirstName(1L, "Ernesto");

        // then
        Assertions.assertThat(studentDTO).isNotEmpty();
    }

    @Test
    @Order(5)
    public void shouldSaveAndReturnANewStudent() {
        Student student = Student.builder()
                .id(1L)
                .firstName("Ernesto")
                .lastName("Acosta")
                .email("")
                .build();

        BDDMockito.given(studentRepository
                .save(student))
                .willReturn(student);

        // when
        StudentDTO studentDTO = studentService
                .saveStudent(new ModelMapper().map(student, StudentDTO.class));

        // then
        Assertions.assertThat(studentDTO.getFirstName()).isEqualTo("Ernesto");
    }

    @Test
    @Order(6)
    public void shouldDeleteStudentById() throws ResourceNotFoundException {
        Student student = Student.builder()
                .id(1L)
                .firstName("Ernesto")
                .lastName("Acosta")
                .email("")
                .build();

        // when
        Mockito.when(studentRepository.findById(1L))
                .thenReturn(Optional.of(student));

        studentService.deleteStudentById(1L);

        // then
        Mockito.verify(studentRepository, Mockito.times(1)).deleteById(1L);
    }
}
